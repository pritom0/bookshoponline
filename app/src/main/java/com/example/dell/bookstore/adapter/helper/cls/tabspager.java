package com.example.dell.bookstore.adapter.helper.cls;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.dell.bookstore.Fragment.BanglaBookFragment;
import com.example.dell.bookstore.Fragment.BusinessBook;
import com.example.dell.bookstore.Fragment.ComputerScienceBook;
import com.example.dell.bookstore.Fragment.EnglishBookFragment;
import com.example.dell.bookstore.Fragment.OthersBook;

public class tabspager extends FragmentStatePagerAdapter {

    String [] title = new String[]{"Bangla Books", "English Books","Computer Science Book","Business Books","Others"};
    public tabspager(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0:
                BanglaBookFragment banglaBookFragment = new BanglaBookFragment();
                return banglaBookFragment;


            case 1:
                EnglishBookFragment englishBookFragment = new EnglishBookFragment();
                return englishBookFragment;

            case 2:
                ComputerScienceBook computerScienceBook = new ComputerScienceBook();
                return computerScienceBook;

            case 3:
                BusinessBook businessBook = new BusinessBook();
                return  businessBook;
            case 4:
                OthersBook othersBook = new OthersBook();
                return  othersBook;


        }

        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }
}