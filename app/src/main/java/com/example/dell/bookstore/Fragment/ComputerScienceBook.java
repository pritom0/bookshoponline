package com.example.dell.bookstore.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dell.bookstore.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ComputerScienceBook extends Fragment {


  View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate( R.layout.fragment_computer_science_book, container, false );
        return  view;
    }

}
