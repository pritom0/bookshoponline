package com.example.dell.bookstore.adapter.helper.cls;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dell.bookstore.Fragment.BanglaBookFragment;
import com.example.dell.bookstore.R;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private ArrayList<BanglaBookFragment> banglaBookFragments = new ArrayList<>(  );
    private Context mContext;
    public CardView cardView;
    public ImageView imageView;
   private String[] ItemName = {"Book1","Book2","Book3","Book4","Book5"};

    private String[] details = {"Book1 details","Book1 details","Book1 details","Book1 details","Book1 details"};

   private int[]  Images = { R.drawable.book1, R.drawable.book1, R.drawable.book1, R.drawable.book1, R.drawable.book1};





    public  RecyclerAdapter (Context context, ArrayList<BanglaBookFragment> banglaBookFragments){
        mContext = context;
        banglaBookFragments = banglaBookFragments;

    }






    //ViewHolder class
    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView ItemName;
        public TextView ItemDetails;
        public ImageView Images;

        public MyViewHolder(View itemView) {
            super( itemView );

            CardView cardView = (CardView) itemView.findViewById( R.id.cardview );

            ItemName = (TextView)itemView.findViewById( R.id.itemname );
            ItemDetails = (TextView)itemView.findViewById ( R.id.id_des );
             Images =(ImageView)itemView.findViewById ( R.id.imageView1 );



        }
    }



    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  itemView= LayoutInflater.from( parent.getContext() ).inflate( R.layout.custom_layout_item,parent,false);
        MyViewHolder vh = new MyViewHolder(  itemView);
        return  vh;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.MyViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return ItemName.length;
    }
}
