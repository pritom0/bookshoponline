package com.example.dell.bookstore.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.example.dell.bookstore.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class EnglishBookFragment extends Fragment {

    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate( R.layout.fragment_english_book, container, false);
        WebView webView =(WebView)view.findViewById( R.id.webview );
        webView.getSettings().setJavaScriptEnabled( true );
        webView.loadUrl("http://gyannbabu.ennvisiodigital.tech/translated_books");
        return  view;
    }

}
